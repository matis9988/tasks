package com.tasks_list.dao;

public interface DataSourceManager {
    void initializeConnenction();

    Object getConnection();
}
