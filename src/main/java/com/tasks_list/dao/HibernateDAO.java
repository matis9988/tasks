package com.tasks_list.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;

public class HibernateDAO<K extends Serializable, V> implements GenericDAO<K, V> {

    Class<V> valueClass;
    private SessionFactory sessionFactory;

    public HibernateDAO(Class<V> entityClass) {
        DataSourceManager sessionManager = new HibernateManager();
        sessionManager.initializeConnenction();
        sessionFactory = (SessionFactory) sessionManager.getConnection();

        valueClass = entityClass;
    }

    public void insert(V value) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(value);
        transaction.commit();
    }

    public V find(K key) {
        Session session = sessionFactory.openSession();
        return (V) session.get(valueClass, key);
    }

    public List<V> findAll() {
        Session session = sessionFactory.openSession();
        return session.createCriteria(valueClass).list();
    }

    public void update(V value) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(value);
        transaction.commit();
    }

    public void delete(V value) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(value);
        transaction.commit();
    }
}
