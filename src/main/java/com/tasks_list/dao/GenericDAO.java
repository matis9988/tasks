package com.tasks_list.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<K extends Serializable, V> {

    void insert(V value);

    V find(K key);

    List<V> findAll();

    void update(V value);

    void delete(V value);

}
