package com.tasks_list.model;

import java.io.Serializable;
import java.util.Collection;

public abstract class Model<T, V> {

    protected Collection<V> data;
    protected T dataSource;

    public Model(T dataSource) {
        this.dataSource = dataSource;
    }

    public abstract void add(V data);
    public abstract V get(Serializable index);
    public Collection<V> get() {
        return this.data;
    }
    public abstract void update(V data);
    public abstract void delete(V data);

}
