package com.tasks_list;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class TasksList extends javafx.application.Application {

    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        setScene(getClass().getResource("/mainView.fxml"));
        primaryStage.show();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setScene(URL fxmlLocation) {
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlLocation);

        try {
            Parent root = (AnchorPane)fxmlLoader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
        } catch (IOException e) {
            System.err.println("Exception occured:" + e);
            e.printStackTrace();
        }
    }

}
