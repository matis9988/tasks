package com.tasks_list.task;

import com.tasks_list.dao.GenericDAO;
import com.tasks_list.model.Model;

import java.io.Serializable;

public class TaskModel extends Model<GenericDAO, Task> {

    public TaskModel(GenericDAO dataSource) {
        super(dataSource);
        super.data = dataSource.findAll();
    }

    @Override
    public void add(Task data) {
        super.dataSource.insert(data);
    }

    @Override
    public Task get(Serializable index) {
        return (Task)super.dataSource.find(index);
    }

    @Override
    public void update(Task data) {
        super.dataSource.update(data);
    }

    @Override
    public void delete(Task data) {
        super.dataSource.delete(data);
    }


}
