package com.tasks_list.controllers;

import com.tasks_list.dao.HibernateDAO;
import com.tasks_list.model.Model;
import com.tasks_list.task.Task;
import com.tasks_list.task.TaskModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class MainViewController implements Initializable, Observer {

    public TableColumn<Task, String> titleColumn;
    public TableColumn<Task, String> descriptionColumn;
    public TableView<Task> taskTable;

    private Model dataBean;
    private ObservableList<Task> listItems;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.dataBean = new TaskModel(new HibernateDAO<>(Task.class));
        listItems = FXCollections.observableArrayList(dataBean.get());

        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        titleColumn.setCellFactory(TextFieldTableCell.<Task>forTableColumn());
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        descriptionColumn.setCellFactory(TextFieldTableCell.<Task>forTableColumn());

        populateTable();
    }

    @Override
    public void update(Observable o, Object arg) {
        dataBean.add(arg);
        listItems.add((Task) arg);

        populateTable();
    }

    private void populateTable() {
        Predicate<Task> predicate = Task::isCompleted;
        taskTable.setItems(listItems.filtered(predicate.negate()));
    }

    public void addButtonAction() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/addTaskDialogView.fxml"));

        Stage dialogStage = new Stage();
        try {
            Scene dialogScene = new Scene(fxmlLoader.load());
            dialogStage.setScene(dialogScene);

            AddTaskDialogController dialogController = fxmlLoader.getController();
            dialogController.addObserver(this);

            dialogStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void commitTitleEdit(TableColumn.CellEditEvent<Task, String> t) {
        Task editedTask = t.getTableView().getItems().get(t.getTablePosition().getRow());
        editedTask.setTitle(t.getNewValue());
        dataBean.update(editedTask);
    }

    public void commitDescriptionEdit(TableColumn.CellEditEvent<Task, String> t) {
        Task editedTask = t.getTableView().getItems().get(t.getTablePosition().getRow());
        editedTask.setDescription(t.getNewValue());
        dataBean.update(editedTask);
    }

    public void removeAction(ActionEvent actionEvent) {
        Task toRemove = taskTable.getFocusModel().getFocusedItem();
        listItems.remove(toRemove);
        dataBean.delete(toRemove);
    }
}
