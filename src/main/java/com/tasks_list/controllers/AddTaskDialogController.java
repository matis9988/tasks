package com.tasks_list.controllers;

import com.tasks_list.task.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

public class AddTaskDialogController extends Observable implements Initializable {

    public TextField titleField;
    public TextArea descriptionArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void addNewTaskAction(ActionEvent actionEvent) {
        Task task = new Task();

        if (titleField.getText().isEmpty() || descriptionArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Title and description fields must not be empty!");
            alert.show();
        } else {
            task.setTitle(titleField.getText());
            task.setDescription(titleField.getText());
            task.setCompleted(false);

            setChanged();
            this.notifyObservers(task);
        }

        closeStage();
    }

    public void cancelAction() {
        closeStage();
    }

    private void closeStage() {
        Stage stage = (Stage) this.titleField.getScene().getWindow();
        stage.close();
    }

}
